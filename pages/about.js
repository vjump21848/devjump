import Layout from "../components/layout";
import Head from "next/head";
const Header = () => (
  <Head>
    <title>
      เกี่ยวกับเว็บไซต์นี้ | จัดทำโดย vjumpkung | IT Front-end Technology SEO
      ในเว็บไซต์เดียว{" "}
    </title>
  </Head>
);
const About = () => (
  <Layout>
    <Header />
    <div className="container mx-auto bg-cool-gray-600 flex justify-center">
      <h1 className="font-black text-6xl text-white">About</h1>
    </div>
  </Layout>
);
export default About;
