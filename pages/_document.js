import Document, { Head, Main, NextScript, Html } from "next/document";
export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charset="utf-8" />
          <link rel="icon" href="/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta
            name="description"
            content="รวมข่าวสาร IT Programming Gadgets ที่สะดวกและรวดเร็วและรวบรวม Project ที่เคยใช้งานจริงเช่น React เป็นต้น"
          />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
