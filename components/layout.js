import { useState } from "react";
import Transition from "./Transition.js";
import Vjump from "../public/image/vjump.svg";
import Link from "./Link";
const Header = () => {
  const [isMobile, setIsMobile] = useState(false);
  return (
    <>
      <div className="relative bg-gray-100" id="header">
        <div className="max-w-screen-xl mx-auto justify-center">
          <div className="relative z-0 pb-3 bg-gray-100 sm:pb-3 md:pb-3 lg:w-full lg:pb-3 xl:pb-3 ">
            <div className="relative pt-3 px-4 sm:px-3 lg:px-3 justify-between items-center">
              <nav className="relative flex items-center justify-between sm:h-10 lg:justify-between">
                <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
                  <div className="flex items-center justify-between w-full md:w-auto">
                    <a href="#" aria-label="Home">
                      <img
                        className="h-10 w-auto sm:h-10"
                        src={Vjump}
                        alt="Logo"
                      />
                    </a>
                    <div id="logo" className="pl-2">
                      deVjump
                    </div>
                    <div className="-mr-2 flex items-center md:hidden">
                      <button
                        type="button"
                        className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                        id="main-menu"
                        aria-label="Main menu"
                        aria-haspopup="true"
                        onClick={() => setIsMobile(!isMobile)}
                      >
                        <svg
                          className="h-6 w-6"
                          stroke="currentColor"
                          fill="none"
                          viewBox="0 0 24 24"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M4 6h16M4 12h16M4 18h16"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="hidden md:block items-center">
                  <Link activeClassName="text-gray-900" href="/">
                    <a className="font-medium text-gray-500 hover:text-gray-900 transition duration-150 ease-in-out">
                      Home
                    </a>
                  </Link>
                  <Link activeClassName="text-gray-900" href="/posts">
                    <a className="ml-8 font-medium text-gray-500 hover:text-gray-900 transition duration-150 ease-in-out">
                      Posts
                    </a>
                  </Link>
                  <Link activeClassName="text-gray-900" href="/guide">
                    <a className="ml-8 font-medium text-gray-500 hover:text-gray-900 transition duration-150 ease-in-out">
                      Guide
                    </a>
                  </Link>
                  <Link activeClassName="text-gray-900" href="/about">
                    <a className="ml-8 font-medium text-gray-500 hover:text-gray-900 transition duration-150 ease-in-out">
                      About
                    </a>
                  </Link>
                </div>
              </nav>
            </div>
            <Transition
              show={isMobile}
              enter="duration-200 ease-out"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="duration-100 ease-in"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
                <div className="rounded-lg shadow-md">
                  <div
                    className="rounded-lg bg-white shadow-xs overflow-hidden"
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="main-menu"
                  >
                    <div className="px-5 pt-4 flex items-center justify-between">
                      <div>
                        <img
                          className="h-8 w-auto"
                          src={Vjump}
                          height="40"
                          alt=""
                        />
                      </div>
                      <div className="-mr-2">
                        <button
                          type="button"
                          className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                          aria-label="Close menu"
                          onClick={() => setIsMobile(!isMobile)}
                        >
                          <svg
                            className="h-6 w-6"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M6 18L18 6M6 6l12 12"
                            />
                          </svg>
                        </button>
                      </div>
                    </div>
                    <div className="px-2 pt-2 pb-3">
                      <a
                        href="#"
                        className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                        role="menuitem"
                      >
                        Home
                      </a>
                      <a
                        href="#"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                        role="menuitem"
                      >
                        Posts
                      </a>
                      <a
                        href="#"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                        role="menuitem"
                      >
                        Guide
                      </a>
                      <a
                        href="#"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                        role="menuitem"
                      >
                        About
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </Transition>
          </div>
        </div>
      </div>
    </>
  );
};
const Layout = (props) => (
  <div>
    <Header />
    {props.children}
  </div>
);

export default Layout;
